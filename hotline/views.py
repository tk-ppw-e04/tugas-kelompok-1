from django.shortcuts import render
from .forms import forms_rumahsakit
from .models import rumahsakit

# Create your views here.
def hotline(request):
    if request.method == 'POST':
        form = forms_rumahsakit(request.POST)
        if form.is_valid():
            form.save()

    form = forms_rumahsakit()
    rumah_sakit = rumahsakit.objects.all()
    context = {
        'rumah_sakit':rumah_sakit,
        'form' : form,
    }
    return render(request, 'HotlinePage.html' , context)



