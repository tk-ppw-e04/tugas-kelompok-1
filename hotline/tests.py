from django.test import TestCase
from django.urls import reverse
from .models import rumahsakit
from .forms import forms_rumahsakit

class UnitTest(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('hotline:hotline'))
        self.assertEqual(response.status_code, 200)

    def test_template_yang_digunakan_dari_halaman_hotline(self):
        response = self.client.get(reverse('hotline:hotline'))
        self.assertTemplateUsed(response, 'HotlinePage.html')

    def test_models_dari_halaman_hotline(self):
        rumahsakit.objects.create(nama='abc' , nomor='123456789')
        n_rs = rumahsakit.objects.all().count()
        self.assertEqual(n_rs,1)

    def test_form_valid_dari_halaman_kegiatan(self):
        data = {'nama':'abcdefgh','nomor':'12345678'}
        form = forms_rumahsakit(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'],"abcdefgh")
        self.assertEqual(form.cleaned_data['nomor'],"12345678")

    def test_post_form_dari_halaman_kegiatan(self):
        test_nama = 'abcdefgh'
        test_nomor = '12345678'
        response = self.client.post('/hotline' , {'nama' : test_nama , 'nomor' : test_nomor})
        self.assertEqual(response.status_code,200)
        form = forms_rumahsakit(data={'nama' : test_nama , 'nomor' : test_nomor})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'],"abcdefgh")
        self.assertEqual(form.cleaned_data['nomor'],"12345678")


    