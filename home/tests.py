from django.test import TestCase, Client
from .models import subscribe_models
from .forms import form_subscribe

# Create your tests here.
class HomeUnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_view_subscribe(self):
        response = Client().get('/')
        isi_html = response.content.decode('utf8')
        self.assertIn("SUBSCRIBE TO COVCARE", isi_html)

    def test_models_subscribe(self):
        subscribe_models.objects.create(nama='John' , email='john@example.com')
        subs = subscribe_models.objects.all().count()
        self.assertEqual(subs,1)

    def test_form_valid(self):
        data = {'nama':'John','email':'john@example.com'}
        form = form_subscribe(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'],"John")
        self.assertEqual(form.cleaned_data['email'],"john@example.com")

    def test_post_form(self):
        test_nama = 'John'
        test_email = 'john@example.com'
        response = self.client.post('/' , {'nama' : test_nama , 'email' : test_email})
        self.assertEqual(response.status_code,200)
        form = form_subscribe(data={'nama' : test_nama , 'email' : test_email})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'],"John")
        self.assertEqual(form.cleaned_data['email'],"john@example.com")
