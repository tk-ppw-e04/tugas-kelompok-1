# Generated by Django 3.1.1 on 2020-11-17 12:52

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_kasus_models'),
    ]

    operations = [
        migrations.AddField(
            model_name='kasus_models',
            name='update',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
