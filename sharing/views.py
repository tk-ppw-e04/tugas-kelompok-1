from django.shortcuts import redirect, render
from django.http import HttpResponse
from .models import Pengalaman
from .forms import SharingForm
from django.core import serializers

# Create your views here.

def sharing_page(request):
    sharing = Pengalaman.objects.all()
    form = SharingForm()
    if request.method == 'POST':  
        forminput = SharingForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return redirect('/sharing-result')
        else:
            return render(request, 'sharing-form.html', {'form':form, 'status': 'failed', 'data' : sharing})       
    else:
        current_data = Pengalaman.objects.all()
        return render(request, 'sharing-form.html', {'form':form, 'data':current_data})

def sharing_result(request):
    sharing = Pengalaman.objects.all()
    qs_json = serializers.serialize('json', sharing)
    return render(request, 'sharing-data.html',{'data':sharing, "chartData":qs_json} )