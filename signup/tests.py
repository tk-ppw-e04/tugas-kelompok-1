from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm

# Create your tests here.

class SignUpUnitTest(TestCase):
    def test_url_signup(self):
        response = Client().get('/accounts/signup')
        self.assertEquals(200,response.status_code)

    def test_nama_templates_signup(self):
        response = Client().get('/accounts/signup')
        self.assertTemplateUsed(response, 'registration/sign-up.html')

    def setUp(self) -> None:
        self.username = 'testuser'
        self.password = 'password'

    def test_signup_page_view_name(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='registration/login.html')


class UserCreationFormTest(TestCase):

    def test_form(self):
        data = {
            'username': 'testuser',
            'password1': 'hello123',
            'password2': 'hello123',
        }

        form = UserCreationForm(data)

        self.assertFalse(form.is_valid())