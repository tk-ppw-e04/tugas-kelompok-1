from django.urls import path
from signup.views import sign_up

app_name = 'signup'

urlpatterns = [
    path('signup', sign_up.as_view(), name='signup'),
]