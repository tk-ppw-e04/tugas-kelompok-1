from django import forms
from .models import Contributors
class forms_contributors(forms.ModelForm):
    class Meta:
        model = Contributors
        fields = ['nama', 'kota']
    nama = forms.CharField(label='Nama', max_length=15, widget=forms.TextInput(attrs={'placeholder':'Contoh: Anonymous', 'class':'form-control'}))
    kota = forms.CharField(label='Kota', max_length=15, widget=forms.TextInput(attrs={'placeholder':'Contoh: Depok', 'class':'form-control'}))