from django.http import response
from django.urls.base import resolve
from django.test import TestCase, Client
from .models import Contributors
from . import views

# Create your tests here.
class DonationUnitTest(TestCase):
    def test_donation_url_is_exist(self):
        response = Client().get('/donation')
        self.assertEqual(response.status_code, 200)

    def test_donation_content(self):
        response = Client().get('/donation')
        self.assertTemplateUsed(response, 'donation.html', 'base.html')
        donations_content = response.content.decode('utf8')
        self.assertIn("Donate", donations_content)
        self.assertIn('<form method="POST" action="savecontributors">', donations_content)
        self.assertIn("Contributors", donations_content)
        self.assertIn('<table>', donations_content)
    
    def test_savecontributors_using_func(self):
        found = resolve('/savecontributors')
        self.assertEqual(found.func, views.savecontributors)

    def test_form_donasi(self):
        self.client.post('/savecontributors', {'nama':'karina', 'kota':'Depok'})
        jumlah = Contributors.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_form_donasi_invalid(self):
        self.client.post('/savecontributors', {})
        jumlah = Contributors.objects.all().count()
        self.assertEqual(jumlah, 0)

    def test_models(self):
        Contributors.objects.create(nama="Karina", kota="Depok")
        jumlah = Contributors.objects.all().count()
        self.assertEqual(jumlah, 1)
    
    def test_donationplaces_content(self):
        response = Client().get('/donationplaces')
        donationsplaces_content = response.content.decode('utf8')
        self.assertIn("Donate", donationsplaces_content)
        self.assertIn("Pilih tempat untuk berdonasi", donationsplaces_content)
        self.assertIn("Thank You For Your Contribution!", donationsplaces_content)
        self.assertIn('<a type="button" id="btn-submit" class="btn btn-info" href="donation">Selesai Berdonasi</a>', donationsplaces_content)