from django.shortcuts import render, redirect
from .models import Contributors
from .forms import forms_contributors

# Create your views here.
def donation(request):
    contributors = Contributors.objects.all()
    total = Contributors.objects.all().count()
    response = {'form':forms_contributors, 'contributors':contributors, 'total':total}
    return render(request, 'donation.html', response)

def savecontributors(request):
    form = forms_contributors(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/donationplaces')
    else:
        return redirect('/donation')

def donationplaces(request):
    return render(request, 'donationplaces.html')

